// Basic event loop includes
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <PixelDevel/PixelDevelEventLoop.h>

// Logging
#include "xAODRootAccess/tools/Message.h"

// xAOD Event information
#include "xAODEventInfo/EventInfo.h"

// xAOD Tracking information
#include "xAODTracking/TrackMeasurementValidationContainer.h"

// C++11 STL
#include <string>
#include <sstream>
#include <iostream>
#include <math.h>

// Couple of constants
#define NPIX 12042240



/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )

// this is needed to distribute the algorithm to the workers
ClassImp(PixelDevelEventLoop)


//================================================================
// Classic Constructor
//================================================================
PixelDevelEventLoop :: PixelDevelEventLoop () : m_barrel_or_endcap(0),
                                                m_layer(0),
                                                m_n_eta_modules(20),
                                                m_n_phi_modules(14),
                                                m_pixel_maps_event_number(873572),
                                                m_pixel_maps_run_number(222222)
{}



//================================================================
// Code to be run at job setup time
//================================================================
EL::StatusCode PixelDevelEventLoop :: setupJob (EL::Job& job)
{
  // Initialize and enable xAOD ROOT Access
  job.useXAOD();
  EL_RETURN_CHECK("setupJob", xAOD::Init());

  return EL::StatusCode::SUCCESS;
}



//================================================================
// Create histograms, output tree/files, etc.
//================================================================
EL::StatusCode PixelDevelEventLoop :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // Histograms showing basic event info
  // mu
  m_h_mu = new TH1F("mu", "mu", 70, 0, 70);
  wk()->addOutput(m_h_mu);

  m_h_pixel_occupancy = new TH1F("pixocc", "Pixel Occupancy", 70, 0, 70);
  wk()->addOutput(m_h_pixel_occupancy);

  // Pixel maps
  for(unsigned int i=0; i < m_n_eta_modules; i++)
  {
    // One vector per phi ring, one phi ring per eta range
    std::vector<TH2*> module_phi_ring;

    for(unsigned int j=0; j < m_n_phi_modules; j++)
    {
      int n_x_bins = 80;
      int n_y_bins = 336;
      int eta_module = i - 10;
      if ((eta_module <= 5)&&(eta_module >= -6)) n_x_bins = 160;

      std::stringstream pixel_map_name;
      pixel_map_name << "pixel_map_etamod_" << i << "_phimod_" << j;

      std::stringstream pixel_map_title;
      pixel_map_title << "eta module = " << eta_module << ", phi module = " << j;

      TH2* pixel_map = new TH2F(pixel_map_name.str().c_str(), pixel_map_title.str().c_str(), n_x_bins, 0, n_x_bins, n_y_bins, 0, n_y_bins);
      wk()->addOutput(pixel_map);

      module_phi_ring.push_back(pixel_map);
    }

    m_h_pixel_maps.push_back(module_phi_ring);
  }

  // number of pixel clusters per mu
  for(unsigned int i=0; i < m_n_eta_modules; i++)
  {
    int eta_module = i - 10;

    std::stringstream name_nocuts;
    name_nocuts << "eta ring : " << eta_module << ", No cuts";
    TH1* new_h_npixclus_eta_ring_nocuts = new TH1F(name_nocuts.str().c_str(), name_nocuts.str().c_str(), 70, 0, 70);
    wk()->addOutput(new_h_npixclus_eta_ring_nocuts);

    std::stringstream name_victoriacuts;
    name_victoriacuts << "eta ring : " << eta_module << ", Victoria's cuts";
    TH1* new_h_npixclus_eta_ring_victoriacuts = new TH1F(name_victoriacuts.str().c_str(), name_victoriacuts.str().c_str(), 70, 0, 70);
    wk()->addOutput(new_h_npixclus_eta_ring_victoriacuts);

    m_h_npixclus_eta_rings_nocuts.push_back(new_h_npixclus_eta_ring_nocuts);
    m_h_npixclus_eta_rings_victoriacuts.push_back(new_h_npixclus_eta_ring_victoriacuts);
  }

  return EL::StatusCode::SUCCESS;
}



//================================================================
// Code executed once per input file
//================================================================
EL::StatusCode PixelDevelEventLoop :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



//================================================================
// Code executed when moving on to a new input file
//================================================================
EL::StatusCode PixelDevelEventLoop :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



//================================================================
// Code to be run after opening file, before entering event loop
// Good place for setting entry lists
//================================================================
EL::StatusCode PixelDevelEventLoop :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  // Obtain event pointer
  xAOD::TEvent* event = wk()->xaodEvent();

  // Initialize the event counter
  m_eventCounter = 0;

  // Print the total number of events in the currently opened file
  Info("initialize", "Number of events = %lli", event->getEntries());

  return EL::StatusCode::SUCCESS;
}



//================================================================
// Code to be run on every single event
//================================================================
EL::StatusCode PixelDevelEventLoop :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Obtain the event pointer
  xAOD::TEvent* event = wk()->xaodEvent();

  // print every 100 events
  if((m_eventCounter % 100) == 0) Info("execute", "Event number = %i", m_eventCounter);
  m_eventCounter++;


  //----------------------------
  // Event information
  //----------------------------
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute", event->retrieve(eventInfo, "EventInfo"));

  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  bool isMC = false;
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) )
  {
    isMC = true; // can do something with this later
  }
  
  // Retrieve mu
  float mu;
  if(isMC) mu = round(eventInfo->actualInteractionsPerCrossing());
  else mu = eventInfo->actualInteractionsPerCrossing();

  m_h_mu->Fill(mu);

  // Retrieve event number/run number
  int event_number = eventInfo->eventNumber();
  int run_number = eventInfo->runNumber();

  //----------------------------
  // Retrieve pixel clusters
  //----------------------------
  const xAOD::TrackMeasurementValidationContainer* pixel_clusters = 0;
  EL_RETURN_CHECK("execute", event->retrieve(pixel_clusters, "PixelClusters"));

  int cluster_counter = 0;

  int n_pixclus_nocuts       = 0;
  int n_pixclus_victoriacuts = 0;

  int n_occupied_pixels_all = 0;

  for(auto cluster_itr = pixel_clusters->begin(); cluster_itr != pixel_clusters->end(); cluster_itr++) 
  {  
    // Narrow down on the IBL: BEC == 0 for barrel, layer == 0 for IBL 
    if(!(((*cluster_itr)->auxdataConst<int>("bec") == m_barrel_or_endcap)&&((*cluster_itr)->auxdataConst<int>("layer") == m_layer))) continue;


    // Retrieve correct module
    auto eta_module = (*cluster_itr)->auxdataConst<int>("eta_module");
    auto phi_module = (*cluster_itr)->auxdataConst<int>("phi_module");


    // Fill npixclus histograms
    m_h_npixclus_eta_rings_nocuts.at(eta_module + 10)->Fill(mu);
    n_pixclus_nocuts++;

    auto cluster_z_size = (*cluster_itr)->auxdataConst<int>("sizeZ");
    auto cluster_size = (*cluster_itr)->auxdataConst<int>("size");

    if((cluster_z_size > 2)&&(cluster_size < (cluster_z_size + 3)))
    {
      m_h_npixclus_eta_rings_victoriacuts.at(eta_module + 10)->Fill(mu);
      n_pixclus_victoriacuts++;
    }


    // Pixel occupancy
    n_occupied_pixels_all += cluster_size;


    // Find the right event to fill the pixel maps
    if(((m_pixel_maps_run_number == run_number)&&(m_pixel_maps_event_number == event_number)))
    {
      TH2* current_module_TH2F = m_h_pixel_maps.at(eta_module + 10).at(phi_module);

      // Retrieve individual pixels
      auto rdo_phi_pixel_index = (*cluster_itr)->auxdataConst<std::vector<int>>("rdo_row");
      auto rdo_eta_pixel_index = (*cluster_itr)->auxdataConst<std::vector<int>>("rdo_col");
      auto rdo_charge          = (*cluster_itr)->auxdataConst<std::vector<float>>("rdo_charge");

      // Retrieve barcodes if available
      std::vector<int> truth_barcode;
      if( (*cluster_itr)->isAvailable< std::vector<int> >( "truth_barcode" ) ) 
        truth_barcode = (*cluster_itr)->auxdata< std::vector<int> >("truth_barcode");

      // Fill the pixel maps
      for(unsigned int i=0; i < rdo_phi_pixel_index.size(); i++)
      {
          current_module_TH2F->Fill(rdo_eta_pixel_index[i], rdo_phi_pixel_index[i], rdo_charge[i]);
      }

      cluster_counter++;
    }

  }

  // Fill pixel occupancy
  if (mu>0)
    m_h_pixel_occupancy->Fill(mu, (float)n_occupied_pixels_all / (float)NPIX);


  return EL::StatusCode::SUCCESS;
}



//================================================================
// Code to be run after all events are processed
//================================================================
EL::StatusCode PixelDevelEventLoop :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



//================================================================
// Code to be run after all events are processed, on a single node
//================================================================
EL::StatusCode PixelDevelEventLoop :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}



//================================================================
// Code to be run after all events are processed, on a single node
//================================================================
EL::StatusCode PixelDevelEventLoop :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
