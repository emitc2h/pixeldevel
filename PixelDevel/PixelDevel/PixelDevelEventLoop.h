#ifndef PixelDevel_PixelDevelEventLoop_H
#define PixelDevel_PixelDevelEventLoop_H

#include <EventLoop/Algorithm.h>

// xAOD ROOT Access infrastructure
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

// ROOT
#include <TH1.h>
#include <TH2.h>

// C++11 STL
#include <vector>

class PixelDevelEventLoop : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  // Which pixel detector section?
  int m_barrel_or_endcap;
  int m_layer;

  // Number of modules
  unsigned int m_n_eta_modules;
  unsigned int m_n_phi_modules;

  int m_pixel_maps_event_number;
  int m_pixel_maps_run_number;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  int m_eventCounter; //!

  // Histograms
  //------------------------------------------------

  // Pixel maps
  std::vector<std::vector<TH2*>> m_h_pixel_maps; //!

  // Number of pixel clusters per IBL eta ring, no cluster shape cuts
  std::vector<TH1*> m_h_npixclus_eta_rings_nocuts; //!

  // Number of pixel clusters per IBL eta ring, no cluster shape cuts
  std::vector<TH1*> m_h_npixclus_eta_rings_victoriacuts; //!

  // Pixel occupancy as a funtion of mu
  TH1* m_h_pixel_occupancy; //!

  // Number of charged truth particles
  TH1* m_h_truth_charged; //!

  // Number of event per mu
  TH1* m_h_mu; //!

  // this is a standard constructor
  PixelDevelEventLoop ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(PixelDevelEventLoop, 1);
};

#endif
