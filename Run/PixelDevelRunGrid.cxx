void PixelDevelRunGrid (const std::string& submitDir)
{
  //===========================================
  // > root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' 'PixelDevelRunGrid.cxx("PixelDevel.Grid.test_v1.0")'
  //==========================================


  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // MC single file:
  //const char* inputFilePath = gSystem->ExpandPathName("/afs/cern.ch/work/m/mtm/workspace/20.0.0.3/WorkArea"); 
  //SH::DiskListLocal list(inputFilePath);
  //SH::scanDir(sh, list, "InDetDxAOD.pool.root");

  //SH::addGrid(sh, "user.mtm.mc14_13TeV.119994.Pythia8_A2MSTW2008LO_minbias_inelastic.recon.AOD.e2743_s1982_s2008_r5872.InDetDxAOD-MC.v0.0_EXT0/");
  SH::addGrid(sh, "user.mtm.mc14_13TeV.159000.ParticleGenerator_nu_E50.recon.AOD.e3122_s1982_s2008_r5872.InDetDxAOD-MC.v0.0_EXT0/");

  // or for data, finding and running over all data "datasets" within the directory:
  // const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/atlas/project/PAT/xAODs/data_p1814/");
  //SH::scanDir (sh, inputFilePath);

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler(sh); // use SampleHandler in this job
  //job.options()->setDouble(EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!

  // add our algorithm to the job, made available by the call to load_packages.C
  PixelDevelEventLoop *alg = new PixelDevelEventLoop;

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd(alg);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  //EL::DirectDriver driver;
  EL::PrunDriver driver;
  driver.options()->setString("nc_outputSampleName", "user.mtm.mc14_13TeV.119994.PixelDevel.test_v2.0");
  driver.options()->setDouble(EL::Job::optGridNFilesPerJob, 1);
  // we can use other drivers to run things on the Grid, with PROOF, etc.

  // process the job using the driver
  driver.submitOnly(job, submitDir);
}