void PixelDevelRun(const std::string& submitDir)
{
  //===========================================
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'PixelDevelRun.cxx ("submitDir")'
  //==========================================


  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // MC single file:
  const char* inputFilePath = gSystem->ExpandPathName("/afs/cern.ch/work/m/mtm/workspace/20.0.0.3/WorkArea"); 
  SH::DiskListLocal list(inputFilePath);
  SH::scanDir(sh, list, "InDetDxAOD.pool.root");

  // or for data, finding and running over all data "datasets" within the directory:
  // const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/atlas/project/PAT/xAODs/data_p1814/");
  //SH::scanDir (sh, inputFilePath);

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler(sh); // use SampleHandler in this job
  //job.options()->setDouble(EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!

  // add our algorithm to the job, made available by the call to load_packages.C
  PixelDevelEventLoop *alg = new PixelDevelEventLoop;

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd(alg);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.

  // process the job using the driver
  driver.submit(job, submitDir);

}